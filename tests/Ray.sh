#!/bin/bash
for number in "Float" "Double" "DoubleDouble" "QuadDouble"
do
  ghc --make -fforce-recomp -O3 -Dnumber="${number}" Ray.hs -o Ray-"${number}"
  time ./Ray-"${number}" >/dev/null
done
