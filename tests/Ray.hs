{-# LANGUAGE CPP #-}
import Control.Monad (forM_)
import Data.Complex (Complex((:+)), mkPolar)
import Data.Ratio ((%))
import Numeric.QD

type N = Int
type Q = Rational
type R = number
type C = Complex R

radius :: R
radius = 2 ** 24

sharpness :: N
sharpness = 4

limit :: N
limit = 64

ray :: Q -> [C]
ray angle = map fst . iterate (step angle) $ (mkPolar radius (2 * pi * fromRational angle), (0, 0))

step :: Q -> (C, (N, N)) -> (C, (N, N))
step angle (c, (k0, j0))
  | j > sharpness = step angle (c, (k0 + 1, 0))
  | otherwise = (c', (k0, j0 + 1))
  where
    k = k0 + 1
    j = j0 + 1
    m = (k - 1) * sharpness + j
    r = radius ** ((1/2) ** (fromIntegral m / fromIntegral sharpness))
    t = mkPolar (r ** (2 ** fromIntegral k0)) ((2 ** fromIntegral k0) * 2 * pi * fromRational angle)
    c' = iterate n c !! limit
    n z = z - (cc - t) / dd
     where
      (cc, dd) = ncnd k
      ncnd 1 = (z, 1)
      ncnd i = let (nc, nd) = ncnd (i - 1) in (nc * nc + z, 2 * nc * nd + 1)

main :: IO ()
main = unsafePreservingFPU $ do
  forM_ [2 .. 7] $ \ i -> do
    let d = 2 ^ i - 1
    forM_ [1 .. (d - 1)] $ \ n -> do
      putStrLn $ "# " ++ show n ++ "/" ++ show d
      let angle = n % d
          rs = ray angle
          printC (x:+y) = putStrLn (show x ++ " " ++ show y)
      mapM_ printC (take 100 rs)
      putStrLn ""
      putStrLn ""
