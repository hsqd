import Data.Ratio ((%))
import Data.IORef (newIORef, readIORef, writeIORef)
import Control.Monad (replicateM_, when)
import System.Environment (getArgs)
import System.Random (randomRIO)
import Numeric.QD (DoubleDouble, QuadDouble)

rand :: IO Rational
rand = do
  let bits = 300 :: Integer
  num <- randomRIO (1, 2^bits)
  den <- randomRIO (1, 2^bits)
  sign <- signum `fmap` randomRIO (-100, 100)
  return $ (sign * num) % den

main :: IO ()
main = do
  [n] <- map read `fmap` getArgs
  dr <- newIORef (0 :: Int)
  qr <- newIORef (0 :: Int)
  replicateM_ n $ do
    f <- rand
    let d = fromRational f :: DoubleDouble
        q = fromRational f :: QuadDouble
        ds = show d
        qs = show q
    when (show (read ds :: DoubleDouble) /= ds) $ do
      m <- readIORef dr
      writeIORef dr $! m + 1
    when (show (read qs :: QuadDouble) /= qs) $ do
      m <- readIORef qr
      writeIORef qr $! m + 1
  ds <- readIORef dr
  qs <- readIORef qr
  print ( 100 * fromIntegral ds / fromIntegral n :: Double
        , 100 * fromIntegral qs / fromIntegral n :: Double )
