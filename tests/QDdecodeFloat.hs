{-# LANGUAGE ScopedTypeVariables #-}

import Data.Bits (shiftL)
import Control.Monad (replicateM_, when)
import System.Random (randomRIO)
import Numeric.QD (QuadDouble, unsafePreservingFPU)

rand :: forall a . RealFloat a => IO a
rand = do
  let ff = floatDigits (0 :: a)
      mMin = 1 `shiftL` (ff - 1)
      mMax = (1 `shiftL` ff) - 1
      rr = floatRange (0 :: a)
  sign <- randomRIO (-1, 1 :: Int)
  if (sign == 0)
    then return $ encodeFloat 0 0
    else do
      bits <- randomRIO (mMin, mMax)
      expo <- randomRIO (fst rr + 300, snd rr - 300)
      return $ encodeFloat (fromIntegral sign * bits) expo

main :: IO ()
main = unsafePreservingFPU $ replicateM_ 1000000 $ do
  f <- rand
  when (uncurry encodeFloat ({-# SCC "decode" #-} decodeFloat f) /= f) $ print (f :: QuadDouble)
