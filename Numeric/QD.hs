{- |
Module      :  Numeric.QD
Copyright   :  (c) Claude Heiland-Allen 2011
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  portable

Higher-level wrapper over bindings to libqd.
-}
module Numeric.QD
  ( module Numeric.QD.DoubleDouble
  , module Numeric.QD.QuadDouble
  ) where

import Numeric.QD.DoubleDouble hiding (toDouble, fromDouble, sqr)
import Numeric.QD.QuadDouble hiding (toDouble, fromDouble, toDoubleDouble, fromDoubleDouble, sqr)
