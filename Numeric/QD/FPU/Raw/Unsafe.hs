{- |
Module      :  Numeric.QD.FPU.Raw.Unsafe
Copyright   :  (c) Claude Heiland-Allen 2011
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  portable

Unsafe FPU manipulation bindings.
-}
{-# LANGUAGE ForeignFunctionInterface #-}
module Numeric.QD.FPU.Raw.Unsafe
  ( fpu_fix_start
  , fpu_fix_end
  ) where

import Foreign (Ptr)
import Foreign.C (CInt)

foreign import ccall unsafe "qd/fpu.h fpu_fix_start" fpu_fix_start :: Ptr CInt -> IO ()
foreign import ccall unsafe "qd/fpu.h fpu_fix_end" fpu_fix_end :: Ptr CInt -> IO ()
