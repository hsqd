{- |
Module      :  Numeric.QD.DoubleDouble.Raw
Copyright   :  (c) Claude Heiland-Allen 2011
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  portable

Bindings to libqd for double-double numbers.
-}
module Numeric.QD.DoubleDouble.Raw
  ( module Numeric.QD.DoubleDouble.Raw.Safe
  ) where

import Numeric.QD.DoubleDouble.Raw.Safe
