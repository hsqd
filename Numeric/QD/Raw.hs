{- |
Module      :  Numeric.QD.Raw
Copyright   :  (c) Claude Heiland-Allen 2011
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  portable

Raw bindings to libqd.
-}
module Numeric.QD.Raw
  ( module Numeric.QD.Raw.Safe
  ) where

import Numeric.QD.Raw.Safe
