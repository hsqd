{- |
Module      :  Numeric.QD.QuadDouble.Safe
Copyright   :  (c) Claude Heiland-Allen 2011
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  portable

Safe bindings to libqd.

These bindings are to foreign wrappers around libqd, which set and
restore the FPU flags correctly.
-}
module Numeric.QD.Raw.Safe
  ( module Numeric.QD.DoubleDouble.Raw.Safe
  , module Numeric.QD.QuadDouble.Raw.Safe
  ) where

import Numeric.QD.DoubleDouble.Raw.Safe
import Numeric.QD.QuadDouble.Raw.Safe
