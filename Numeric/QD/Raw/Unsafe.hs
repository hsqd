{- |
Module      :  Numeric.QD.Raw.Unsafe
Copyright   :  (c) Claude Heiland-Allen 2011
License     :  BSD3

Maintainer  :  claude@mathr.co.uk
Stability   :  unstable
Portability :  portable

Unsafe bindings to libqd.

It is strongly recommended to use instead the
@'Numeric.QD.Raw.Safe'@ wrappers.
-}
module Numeric.QD.Raw.Unsafe
  ( module Numeric.QD.DoubleDouble.Raw.Unsafe
  , module Numeric.QD.FPU.Raw.Unsafe
  , module Numeric.QD.QuadDouble.Raw.Unsafe
  ) where

import Numeric.QD.DoubleDouble.Raw.Unsafe
import Numeric.QD.FPU.Raw.Unsafe
import Numeric.QD.QuadDouble.Raw.Unsafe
