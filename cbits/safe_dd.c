#include <qd/fpu.h>
#include <qd/c_dd.h>

#define PRESERVE(S) do { unsigned int safe_fpu; fpu_fix_start(&safe_fpu); S; fpu_fix_end(&safe_fpu); } while(0)

/* dd = dd OP dd */
#define SAFE(F) void safe_dd_##F(const double *a, const double *b, double *c) { PRESERVE(c_dd_##F(a,b,c)); }
SAFE(add)
SAFE(sub)
SAFE(mul)
SAFE(div)
SAFE(atan2)
#undef SAFE

/* dd = d OP dd */
#define SAFE(F) void safe_dd_##F##_d_dd(double a, const double *b, double *c) { PRESERVE(c_dd_##F##_d_dd(a,b,c)); }
SAFE(add)
SAFE(sub)
SAFE(mul)
SAFE(div)
#undef SAFE

/* dd = dd OP d */
#define SAFE(F) void safe_dd_##F##_dd_d(const double *a, double b, double *c) { PRESERVE(c_dd_##F##_dd_d(a,b,c)); }
SAFE(add)
SAFE(sub)
SAFE(mul)
SAFE(div)
#undef SAFE

/* dd = OP dd */
#define SAFE(F) void safe_dd_##F(const double *a, double *b) { PRESERVE(c_dd_##F(a,b)); }
SAFE(copy)
SAFE(sqrt)
SAFE(sqr)
SAFE(abs)
SAFE(nint)
SAFE(aint)
SAFE(floor)
SAFE(ceil)
SAFE(exp)
SAFE(log)
SAFE(log10)
SAFE(sin)
SAFE(cos)
SAFE(tan)
SAFE(asin)
SAFE(acos)
SAFE(atan)
SAFE(sinh)
SAFE(cosh)
SAFE(tanh)
SAFE(asinh)
SAFE(acosh)
SAFE(atanh)
SAFE(neg)
#undef SAFE

/* misc */
void safe_dd_copy_d(double a, double *b) { PRESERVE(c_dd_copy_d(a,b)); }
void safe_dd_npwr(const double *a, int b, double *c) { PRESERVE(c_dd_npwr(a,b,c)); }
void safe_dd_nroot(const double *a, int b, double *c) { PRESERVE(c_dd_nroot(a,b,c)); }
void safe_dd_sincos(const double *a, double *b, double *c) { PRESERVE(c_dd_sincos(a,b,c)); }
void safe_dd_sincosh(const double *a, double *b, double *c) { PRESERVE(c_dd_sincosh(a,b,c)); }
void safe_dd_comp(const double *a, const double *b, int *c) { PRESERVE(c_dd_comp(a,b,c)); }
void safe_dd_comp_dd_d(const double *a, double b, int *c) { PRESERVE(c_dd_comp_dd_d(a,b,c)); }
void safe_dd_comp_d_dd(double a, const double *b, int *c) { PRESERVE(c_dd_comp_d_dd(a,b,c)); }
void safe_dd_read(const char *a, double *b) { PRESERVE(c_dd_read(a,b)); }
void safe_dd_swrite(const double *a, int b, char *c, int d) { PRESERVE(c_dd_swrite(a,b,c,d)); }
void safe_dd_write(const double *a) { PRESERVE(c_dd_write(a)); }
void safe_dd_rand(double *a) { PRESERVE(c_dd_rand(a)); }
void safe_dd_pi(double *a) { PRESERVE(c_dd_pi(a)); }

#undef PRESERVE
