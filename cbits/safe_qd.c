#include <qd/fpu.h>
#include <qd/c_qd.h>

#define PRESERVE(S) do { unsigned int safe_fpu; fpu_fix_start(&safe_fpu); S; fpu_fix_end(&safe_fpu); } while(0)

/* qd = qd OP qd */
#define SAFE(F) void safe_qd_##F(const double *a, const double *b, double *c) { PRESERVE(c_qd_##F(a,b,c)); }
SAFE(add)
SAFE(sub)
SAFE(mul)
SAFE(div)
SAFE(atan2)
#undef SAFE

/* qd = dd OP qd */
#define SAFE(F) void safe_qd_##F##_dd_qd(const double *a, const double *b, double *c) { PRESERVE(c_qd_##F##_dd_qd(a,b,c)); }
SAFE(add)
SAFE(sub)
SAFE(mul)
SAFE(div)
#undef SAFE

/* qd = qd OP dd */
#define SAFE(F) void safe_qd_##F##_qd_dd(const double *a, const double *b, double *c) { PRESERVE(c_qd_##F##_qd_dd(a,b,c)); }
SAFE(add)
SAFE(sub)
SAFE(mul)
SAFE(div)
#undef SAFE

/* dd = d OP qd */
#define SAFE(F) void safe_qd_##F##_d_qd(double a, const double *b, double *c) { PRESERVE(c_qd_##F##_d_qd(a,b,c)); }
SAFE(add)
SAFE(sub)
SAFE(mul)
SAFE(div)
#undef SAFE

/* dd = qd OP d */
#define SAFE(F) void safe_qd_##F##_qd_d(const double *a, double b, double *c) { PRESERVE(c_qd_##F##_qd_d(a,b,c)); }
SAFE(add)
SAFE(sub)
SAFE(mul)
SAFE(div)
#undef SAFE

/* qd OP = qd */
#define SAFE(F) void safe_qd_self##F(const double *a, double *b) { PRESERVE(c_qd_self##F(a,b)); }
SAFE(add)
SAFE(sub)
SAFE(mul)
SAFE(div)
#undef SAFE

/* qd OP = dd */
#define SAFE(F) void safe_qd_self##F##_dd(const double *a, double *b) { PRESERVE(c_qd_self##F##_dd(a,b)); }
SAFE(add)
SAFE(sub)
SAFE(mul)
SAFE(div)
#undef SAFE

/* qd OP = d */
#define SAFE(F) void safe_qd_self##F##_d(double a, double *b) { PRESERVE(c_qd_self##F##_d(a,b)); }
SAFE(add)
SAFE(sub)
SAFE(mul)
SAFE(div)
#undef SAFE

/* qd = OP qd */
#define SAFE(F) void safe_qd_##F(const double *a, double *b) { PRESERVE(c_qd_##F(a,b)); }
SAFE(copy)
SAFE(sqrt)
SAFE(sqr)
SAFE(abs)
SAFE(nint)
SAFE(aint)
SAFE(floor)
SAFE(ceil)
SAFE(exp)
SAFE(log)
SAFE(log10)
SAFE(sin)
SAFE(cos)
SAFE(tan)
SAFE(asin)
SAFE(acos)
SAFE(atan)
SAFE(sinh)
SAFE(cosh)
SAFE(tanh)
SAFE(asinh)
SAFE(acosh)
SAFE(atanh)
SAFE(neg)
#undef SAFE

/* misc */
void safe_qd_copy_dd(const double *a, double *b) { PRESERVE(c_qd_copy_dd(a,b)); }
void safe_qd_copy_d(double a, double *b) { PRESERVE(c_qd_copy_d(a,b)); }
void safe_qd_npwr(const double *a, int b, double *c) { PRESERVE(c_qd_npwr(a,b,c)); }
void safe_qd_nroot(const double *a, int b, double *c) { PRESERVE(c_qd_nroot(a,b,c)); }
void safe_qd_sincos(const double *a, double *b, double *c) { PRESERVE(c_qd_sincos(a,b,c)); }
void safe_qd_sincosh(const double *a, double *b, double *c) { PRESERVE(c_qd_sincosh(a,b,c)); }
void safe_qd_comp(const double *a, const double *b, int *c) { PRESERVE(c_qd_comp(a,b,c)); }
void safe_qd_comp_qd_d(const double *a, double b, int *c) { PRESERVE(c_qd_comp_qd_d(a,b,c)); }
void safe_qd_comp_d_qd(double a, const double *b, int *c) { PRESERVE(c_qd_comp_d_qd(a,b,c)); }
void safe_qd_read(const char *a, double *b) { PRESERVE(c_qd_read(a,b)); }
void safe_qd_swrite(const double *a, int b, char *c, int d) { PRESERVE(c_qd_swrite(a,b,c,d)); }
void safe_qd_write(const double *a) { PRESERVE(c_qd_write(a)); }
void safe_qd_rand(double *a) { PRESERVE(c_qd_rand(a)); }
void safe_qd_pi(double *a) { PRESERVE(c_qd_pi(a)); }

#undef PRESERVE
